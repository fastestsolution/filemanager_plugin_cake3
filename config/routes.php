<?php
use Cake\Routing\Router;
use Cake\Core\Configure;


Router::scope('/filemanager/', function ($routes) {
	
	$routes->connect('/getFtpList/*', ['controller' => 'FileManagers', 'action' => 'getFtpList','plugin'=>'FileManager']);
	$routes->connect('/renameFile/*', ['controller' => 'FileManagers', 'action' => 'renameFile','plugin'=>'FileManager']);
	$routes->connect('/deleteFile/*', ['controller' => 'FileManagers', 'action' => 'deleteFile','plugin'=>'FileManager']);
	$routes->connect('/createFolder/*', ['controller' => 'FileManagers', 'action' => 'createFolder','plugin'=>'FileManager']);
	$routes->connect('/upload/*', ['controller' => 'FileManagers', 'action' => 'upload','plugin'=>'FileManager']);
	$routes->connect('/ckeditor/*', ['controller' => 'FileManagers', 'action' => 'ckeditor','plugin'=>'FileManager']);
	$routes->connect('/', ['controller' => 'FileManagers', 'plugin'=>'FileManager']);
	
	//$routes->fallbacks('DashedRoute');
});	

