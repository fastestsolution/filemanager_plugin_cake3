var FileManager = this.FileManager = new Class({
	Implements:[Options,Events],
	historyUrl: '',
	
	// init fce
	initialize:function(options){
		if ($('newFolderName')){
			this.initManager();
			this.getHistory();
			this.loadData(this.historyUrl !=''?this.historyUrl:'getFtpList/');
			
			this.initGlobalEvents();
			this.initUploader();
			
		}
	},
	
	/**
	* init manager on load
	*/
	initManager: function(){
		if ($('filesTable')){
			this.template = Tempo.prepare("filesTable");
			$('previewFileFrame').src = '';
		}
		//this.fstAlert('aaa',true);
		//console.log(this.template);
	},
	
	/**
	* push to history
	*/
	setHistory(url){
		console.log('set url',url);
		history.replaceState(null, null, '#'+url);
			
	},
	
	/**
	* get from history
	*/
	getHistory(url){
		if (window.location.hash){
			this.historyUrl = (window.location.hash.substring(1));
			console.log('history url',this.historyUrl);
		}
		
	},
	
	/**
	* init uploader file
	*/
	initUploader: function(){
		// Create the file uploader
		var upload = new Form.Upload('file', {
			dropMsg: "Přetáhněte zde soubory",
			onComplete: function(json){
				json = JSON.decode(json);
				if (json.result == true){
					this.fstAlert(json.message);
					this.loadData('getFtpList/'+this.currentPath);
					$('uploadList').empty();
					$('progress').setStyle('display','none');
				} else {
					this.fstAlert(json.message,true);
					
				}
			}.bind(this)
		});

		// Use iFrameFormRequest, which posts to iFrame 
		if (!upload.isModern()) {
			new iFrameFormRequest('uploadForm', {
				onComplete: function(response){
					alert('Files uploaded!');
				}
			});
		}
	},
	
	
	/**
	* select file to CK editor
	*/
	selectFileCkEditor: function(fileUrl){
		console.log(fileUrl);
		function getUrlParam( paramName ) {
            var reParam = new RegExp( '(?:[\?&]|&)' + paramName + '=([^&]+)', 'i' );
            var match = window.location.search.match( reParam );

            return ( match && match.length > 1 ) ? match[1] : null;
        }
        // Simulate user action of selecting a file to be returned to CKEditor.
        
		
        var funcNum = getUrlParam( 'CKEditorFuncNum' );
			//console.log(funcNum);
			//var fileUrl = '/path/to/file.txt';
			if (window.opener){
				window.opener.CKEDITOR.tools.callFunction(funcNum,fileUrl);
			} else {
				alert(fileUrl);
			}
        //console.log(window.opener.CKEDITOR.tools);
		window.close();
        
	},
	
	/**
	* hezke alerty
	*/
	fstAlert: function(text,error=false){
		$('fstAlert').set('text',text);
		$('fstAlert').addClass('show');
		if (!error){
			$('fstAlert').addClass('alert-success');
		} else {
			$('fstAlert').addClass('alert-danger');
		
		}
		(function(){
			$('fstAlert').removeClass('show');
		}).delay(2500);
		
	},
	
	/**
	* init global events to elements
	*/
	initGlobalEvents: function(){
		$('newFolder').addEvent('click',function(e){
			$('newFolderName').toggleClass('none');
			if (!$('newFolderName').hasClass('none')){
				$('newFolderName').value = '';
				$('newFolderName').focus();
			}
		});
		
		$('previewFileResize').addEvent('click',function(e){
			$('previewFileAbs').toggleClass('full');
		});
		/*
		var myResize = $('previewFileAbs').makeResizable({
			handle : $('previewFileResize'),
			invert :true,
			onComplete: function(){
			}
		});
		*/
		
		$('newFolderName').addEvent('keydown',function(e){
			if (e.key == 'enter'){
				var params = {
					url:'/filemanager/createFolder/',
					reload_url: this.currentPath,
					data: {
						folder: e.target.value,
						path: this.currentPath,
					}
				}
				this.sendRequestToServer(params);
				//console.log(this.currentPath);
			}
		}.bind(this));
	},

	/**
	* load data from FTP by path
	*/
	loadData: function(url='getFtpList/'){
		console.log('get data url',url);
		this.setHistory(url);
			
		if (this.historyUrl && this.historyUrl != '' && url != 'getFtpList/'){
			console.log('change url',this.historyUrl);
			//url = this.historyUrl;
		}
		//return false;
		
		//console.log('get url',url);
		
		$('newFolderName').value = '';
		$('newFolderName').addClass('none');
		//console.log('save history',url.substring(24,500));
		
		
		//this.setHistory(url.substring(24,500));
		
		this.filePreloader();
		
		this.load_req = new Request.JSON({
			url:url,
			onError: this.req_error = (function(data){
				//button_preloader(button);
					
			}).bind(this),

			onComplete :(function(json){
				if (json.result == true){
					this.currentPath = json.currentPath;
					console.log(this.currentPath);
					$('uploadForm').set('action','/filemanager/upload/?path='+this.currentPath);
					this.template.clear();
					//this.template.render(json.data);
					this.template.when(TempoEvent.Types.RENDER_COMPLETE, function (event) {
						this.initFileEvents();
						this.filePreloader(true);
					}.bind(this)).render(json.data);
					
					$('currentPathShow').set('text',json.path);
		
				}
			}).bind(this)
		});
		this.load_req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		this.load_req.send();
				
	},
	
	/**
	* init events to link on file
	*/
	initFileEvents: function(){
		// change folder or open file
		$('filesTable').getElements('.file_change').removeEvents('click');
		$('filesTable').getElements('.file_change').addEvent('click',function(e){
			e.stop();
			var url = e.target.get('href');
			var path = e.target.get('data-path-preview');
			
			// pokud je adresar
			if (e.target.hasClass('type_directory')){
				
				this.loadData(url);
			}
			// pokud je soubor
			if (e.target.hasClass('type_file')){
				$('previewFileFrame').set('src',path);
				$('previewFileFrame').addClass('show');
				console.log(path);
			}
		}.bind(this));
		
		// rename file / folder
		$('filesTable').getElements('.fileRename').removeEvents('click');
		$('filesTable').getElements('.fileRename').addEvent('click',function(e){
			e.stop();
			this.filePath = e.target.get('data-file');
			var tr = e.target.getParent('tr');
			
			var nameEl = this.nameEl = tr.getElement('.file_change');
			this.hideNameElement(nameEl);
			
			
			//console.log(path);
		}.bind(this));
		
		// delete file / folder
		$('filesTable').getElements('.fileDelete').removeEvents('click');
		$('filesTable').getElements('.fileDelete').addEvent('click',function(e){
			e.stop();
			if (confirm('Opravdu smazat?')){
				this.filePath = e.target.get('data-file');
				var tr = e.target.getParent('tr');
				var params = {
					url:'/filemanager/deleteFile/'+(e.target.hasClass('type_directory')?'true':''),
					tr_delete: tr,
					data: {
						file: this.filePath,
					}
				}
				this.sendRequestToServer(params);
				//var nameEl = this.nameEl = tr.getElement('.file_change');
				//this.hideNameElement(nameEl);
			}
			
			
			//console.log(path);
		}.bind(this));
		
		// select file 
		$('filesTable').getElements('.fileSelect').removeEvents('click');
		$('filesTable').getElements('.fileSelect').addEvent('click',function(e){
			e.stop();
				this.selectFileCkEditor(e.target.get('data-webPath'));
				//alert(this.filePath);
			
			
			//console.log(path);
		}.bind(this));
		
	},
	
	/**
	* hide / show fileName element
	*/
	hideNameElement: function(element,hide=false){
		if (element.hasClass('none')){
			element.removeClass('none');
			element.set('text',this.editInput.value);
			
			var params = {
				url:'/filemanager/renameFile/',
				element: element,
				data: {
					oldFile: this.filePath.replace(this.editInput.get('data-old-name'),'').replace('//','/')+this.editInput.get('data-old-name'),
					newFile: this.filePath.replace(this.editInput.get('data-old-name'),'').replace('//','/')+this.editInput.value,
					newName: this.editInput.value,
				}
			}
			this.sendRequestToServer(params);
			
			
			if (this.editInput){
				this.editInput.destroy();
			}
			
		} else {
			element.addClass('none');
			var editInput = this.editInput = new Element('input',{'data-path':this.filePath,'class':'edit_input','value':this.nameEl.get('text'),'data-old-name':this.nameEl.get('text')}).inject(this.nameEl.getParent('td'));
			editInput.focus();
			this.editInputEvents(editInput);
		
		}
			
	},
	
	/**
	* send request to server
	*/
	sendRequestToServer: function(params){
		this.send_req = new Request.JSON({
			url:params.url,
			data: params.data,
			onError: this.send_error = (function(data){
					
			}).bind(this),

			onComplete :(function(json){
				if (json.result == true){
					if (params.element){
						params.element.set('text',json.file);
					}
					if (params.tr_delete){
						params.tr_delete.destroy();
					}
					if (params.reload_url){
						this.loadData('getFtpList/'+params.reload_url);	
					}
					if (json.message){
						this.fstAlert(json.message);
					}
				} else {
					this.fstAlert(json.message,true);
				}
			}).bind(this)
		});
		this.send_req.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		this.send_req.send();
	},
	
	/**
	* edit input event
	*/
	editInputEvents: function(element){
		element.addEvent('keydown',function(e){
			if (e.key == 'enter'){
				this.hideNameElement(this.nameEl);
			}
		}.bind(this));
	},
	
	/**
	* file preloader
	*/
	filePreloader: function(hide=false){
		if (!hide){
			$('filesTable').addClass('preloader');
		} else {
			$('filesTable').removeClass('preloader');
		}
	},
	
	
	
	
	
	
});
window.addEvent('domready',function(){
	var fileManager = new FileManager();

	
});

