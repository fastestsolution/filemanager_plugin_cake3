## FileManager plugin for CakePHP 3+
## Created Jakub Tysoň - Fastest Solution
## Date: 12.4.2018


###############
## Installation
###############

## 1 Rozbalit z git repository 
	do /plugins/ pouzit prikaz: git clone "repositor" FileManager

## 2. do /config/bootstrap.php vlozit na konec:

	/**
	* INIT FileManager
	*/
	require_once ROOT . DS .'plugins/FileManager/src/filemanager_config.php';
	Configure::write("file_manager_config",$file_manager_config);
	Plugin::load('FileManager', ['routes' => true, 'autoload' => true]);
	Plugin::routes();

## 3. upravit /plugins/FileManager/config/config.php dle aktualniho projektu

## 4. upravit pripojeni k SESSION dle projektu 
	v souboru /plugins/FileManager/src/Controller/AppController.php radek cislo: 15


	public function initSession(){
		
		require_once('../plugins/FileManager/config/config.php');
		
		// toto je docasne vytvoreni session, SESSION_NAME je z /config/config.php
		$this->Session->write(SESSION_NAME,(object)[
			'ftp'=>[
				'host'=>'109.123.216.75',
				'user'=>'fastestsolution_car',
				'password'=>'car159',
			],
			'domain'=>'http://p-carprogram.fastest.cz'
		]);
		
		
		// pokud neni session s ftp udaji
		if(!$this->Session->check(SESSION_NAME)){
			die('Neni nastaven uzivatel pro zobrazeni souboru');
			
		} else {
			$this->ftpData = $this->Session->read(SESSION_NAME);
		}
		
	}


