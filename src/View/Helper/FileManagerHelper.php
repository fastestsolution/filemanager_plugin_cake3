<?php

namespace Shop\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\View\Helper\HtmlHelper;
use Cake\View\Helper\FastestHelper;

class ShopHelper extends Helper
{

	public $helpers = ['Html','Form','Fastest'];
  
  
	// add basket element
	function addBasketElement($opt=null){
		/*
		$opt = [
			'id'=>1,
			'varianta_select'=>'',
			'variants_list'=>'',
			'price'=>555,
			'ks'=>2,
		];
		*/
		$out = $this->Form->create('',['url'=>'/'.ORDERS_LINK.'/add_basket/']);
		$out .= $this->Form->input('count', ['label'=>false,'placeholder' => __d('shop','Ks'),'class'=>'form-control addBasket_ks','value'=>$opt['ks'],'type'=>(isset($opt['hidden_ks']) && $opt['hidden_ks'] == true?'hidden':'text'),'id'=>'count'.$opt['id']]); 
		if (!isset($opt['btn_class'])) $opt['btn_class'] = 'btn-lg';
		
		$out .= $this->Form->hidden('id', ['label'=>false,'value'=>$opt['id']]); 
		$out .= $this->Form->hidden('price', ['label'=>false,'value'=>$opt['price']]); 
		if (isset($opt['variants_list'])){
			$out .= $this->Form->hidden('variants_list', ['label'=>false,'value'=>json_encode($opt['variants_list']),'id'=>'','class'=>'variants_list']); 
		}
		
		// varianty pro vypis produktu
		if (isset($opt['variants_select_list_item']) && !empty($opt['variants_select_list_item'])){
			$out .= '<ul class="varianta_list_item none">';
			$out .= '<li class="title">'.__d('shop','Vyberte variantu').' <span class="variant_close fa fa-close"</li>';
			foreach($opt['variants_select_list_item'] AS $v){
				$out.= '<li class="varianta_list_select clearfix" data-id="'.$v['id'].'">'.$v['name'].'<span class="price">'.$this->Fastest->price($v['price_vat']).'</span></li>';
			}
			$out.= '</ul>';
			$out .= $this->Form->input('variants_select_list', ['label'=>false,'type'=>'text','class'=>'variants_select_list_el none']); 
		}
		
		// varianty pro detail produktu
		if (isset($opt['variants_select_list']) && !empty($opt['variants_select_list'])){
			$out .= $this->Form->input('variants_select_list', ['label'=>false,'options'=>$opt['variants_select_list'],'type'=>'select','empty'=>false,'class'=>'form-control change_varianta',]); 
		}
		
		$out .= $this->Form->button(__d('shop','Přidat do košíku'), ['class' => 'btn '.$opt['btn_class'].' btn-default add_to_basket']);
		$out .= $this->Form->end();
		return $out;
	}
	
	
	// add basket element
	function selectTransportElement($opt=null){
		if (isset($opt['data'])){
			$dop = $opt['data'];
			
		}
		$basket_products = $opt['basket_products'];
		if (isset($opt['data_list'])){
			$data_list = $opt['data_list'];
			
		}
		$out = '';
		
			// radio button
			if (isset($opt['select_type']) && $opt['select_type'] == 'radio'){
				$inputId = $opt['name'].$dop->id;
				
				$out .= '<input type="radio"  
				name="'.$opt['name'].'" 
				data-id="'.$dop->id.'" 
				data-free-shipping="'.$dop->free_shipping_order_price.'" 
				data-price="'.$dop->price_vat.'" 
				data-price_default="'.$dop->price_vat.'" 
				data-type="'.$opt['type'].'" 
				id="'.$inputId.'" 
				value="'.$dop->id.'"
				class="check_'.$opt['type'].' add_transport" 
				data-payment_ids="'.json_encode($dop->payment_ids).'" '.((isset($basket_products[$opt['type']]) && $basket_products[$opt['type']]['id'] == $dop->id)?'checked':'').' />';
				$out .= '<label for="'.$inputId.'"></label>';	
		
			// select tag	
			} else {
			
				$options = [];
				foreach($data_list AS $dop){
					$options[$dop->id] = [
						'text'=>$dop->name,
						'value'=>$dop->id,
						'class'=>'check_'.$opt['type'].' add_transport '.(($opt['type'] == 'payment')?'payment_line payment_line_'.$dop->id:' check_transport'),
						'data-payment_ids'=>json_encode($dop->payment_ids),
						'data-id'=>$dop->id, 
						'data-free-shipping'=>$dop->free_shipping_order_price,
						'data-price'=>$dop->price_vat,
						'data-price_default'=>$dop->price_vat,
						'data-type'=>$opt['type'],
						];
				}
				//pr($options);
				$out .= $this->Form->input($opt['name'],['options'=>$options,'type'=>'select','class'=>'form-control','label'=>$opt['label'],'value'=>((isset($basket_products[$opt['type']]['id']))?$basket_products[$opt['type']]['id']:'')]);
			}
		return $out;
	}
	
	

}