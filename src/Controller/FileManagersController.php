<?php
namespace FileManager\Controller;

use FileManager\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\Time;
use lessc\lessc;
use Cake\Cache\Cache;
//use Cake\Cache\Cache;

class FileManagersController extends AppController
{		
	var $enabledExt = array('pdf','gif','png','jpg','doc','xls','docx','xlsx','zip','txt','csv');
	var $data_upload = [];
	var $mime_types = array(
		'stl' 		=>	array('application/SLA'),
		'step' 		=>	array('application/STEP'),
		'stp' 		=>	array('application/STEP'),
		'dwg' 		=>	array('application/acad'),
		'ez' 		=>	array('application/andrew-inset'),
		'ccad' 		=>	array('application/clariscad'),
		'drw' 		=>	array('application/drafting'),
		'tsp' 		=>	array('application/dsptype'),
		'dxf' 		=>	array('application/dxf'),
		'unv' 		=>	array('application/i-deas'),
		'jar' 		=>	array('application/java-archive'),
		'hqx' 		=>	array('application/mac-binhex40'),
		'cpt' 		=>	array('application/mac-compactpro'),
		'pot' 		=>	array('application/vnd.ms-powerpoint'),
		'pps' 		=>	array('application/vnd.ms-powerpoint','application/octet-stream'),
		'ppt' 		=>	array('application/vnd.ms-powerpoint','application/octet-stream',),
		'pptx' 		=>	array('application/vnd.ms-powerpoint','application/octet-stream',),
		'ppsx' 		=>	array('application/vnd.ms-powerpoint','application/octet-stream','application/vnd.openxmlformats-officedocument.presentationml.slideshow'),
		'ppz' 		=>	array('application/vnd.ms-powerpoint','application/octet-stream',),
		'bin' 		=>	array('application/octet-stream'),
		'class' 	=>	array('application/octet-stream'),
		'dms' 		=>	array('application/octet-stream'),
		'exe' 		=>	array('application/octet-stream'),
		'lha' 		=>	array('application/octet-stream'),
		'lzh' 		=>	array('application/octet-stream'),
		'oda' 		=>	array('application/oda'),
		'ogg' 		=>	array('application/ogg'),
		'ogm' 		=>	array('application/ogg'),
		'pdf' 		=>	array('application/pdf','application/x-pdf','application/download','application/force-download','application/x-download'),
		'pgp' 		=>	array('application/pgp'),
		'ai' 		=>	array('application/postscript'),
		'eps' 		=>	array('application/postscript'),
		'ps' 		=>	array('application/postscript'),
		'prt' 		=>	array('application/pro_eng'),
		'rtf' 		=>	array('application/rtf'),
		'set' 		=>	array('application/set'),
		'smi' 		=>	array('application/smil'),
		'smil' 		=>	array('application/smil'),
		'sol' 		=>	array('application/solids'),
		'vda' 		=>	array('application/vda'),
		'mif' 		=>	array('application/vnd.mif'),
		'xlc' 		=>	array('application/vnd.ms-excel'),
		'xll' 		=>	array('application/vnd.ms-excel'),
		'xlm' 		=>	array('application/vnd.ms-excel'),
		'xlsm' 		=>	array('application/vnd.ms-excel.sheet.macroenabled.12'),
		//'xls' 		=>	array('application/vnd.ms-excel'),
		'xlw' 		=>	array('application/vnd.ms-excel'),
		'cod' 		=>	array('application/vnd.rim.cod'),
		'arj' 		=>	array('application/x-arj-compressed'),
		'bcpio' 	=>	array('application/x-bcpio'),
		'vcd' 		=>	array('application/x-cdlink'),
		'pgn' 		=>	array('application/x-chess-pgn'),
		'cpio' 		=>	array('application/x-cpio'),
		'csh' 		=>	array('application/x-csh'),
		'deb' 		=>	array('application/x-debian-package'),
		'dcr' 		=>	array('application/x-director'),
		'dir' 		=>	array('application/x-director'),
		'dxr'		=>	array('application/x-director'),
		'dvi' 		=>	array('application/x-dvi'),
		'pre' 		=>	array('application/x-freelance'),
		'spl' 		=>	array('application/x-futuresplash'),
		'gtar' 		=>	array('application/x-gtar'),
		'gz' 		=>	array('application/x-gunzip'),
		'gz' 		=>	array('application/x-gzipv'),
		'hdf' 		=>	array('application/x-hdf'),
		'ipx' 		=>	array('application/x-ipix'),
		'ips' 		=>	array('application/x-ipscript'),
		'js' 		=>	array('application/x-javascript'),
		'skd' 		=>	array('application/x-koan'),
		'skm' 		=>	array('application/x-koan'),
		'skp' 		=>	array('application/x-koan'),
		'skt' 		=>	array('application/x-koan'),
		'latex' 	=>	array('application/x-latex'),
		'lsp' 		=>	array('application/x-lisp'),
		'scm' 		=>	array('application/x-lotusscreencam'),
		'mif' 		=>	array('application/x-mif'),
		'bat' 		=>	array('application/x-msdos-program'),
		'com' 		=>	array('application/x-msdos-program'),
		'exe' 		=>	array('application/x-msdos-program'),
		'cdf' 		=>	array('application/x-netcdf'),
		'nc' 		=>	array('application/x-netcdf'),
		'pl' 		=>	array('application/x-perl'),
		'pm' 		=>	array('application/x-perl'),
		'rar' 		=>	array('application/x-rar-compressed'),
		'sh' 		=>	array('application/x-sh'),
		'shar' 		=>	array('application/x-shar'),
		'swf' 		=>	array('application/x-shockwave-flash'),
		'sit' 		=>	array('application/x-stuffit'),
		'sv4cpio' 	=>	array('application/x-sv4cpio'),
		'sv4crc' 	=>	array('application/x-sv4crc'),
		'tar.gz' 	=>	array('application/x-tar-gz'),
		'tgz' 		=>	array('application/x-tar-gz'),
		'tar' 		=>	array('application/x-tar'),
		'tcl' 		=>	array('application/x-tcl'),
		'tex' 		=>	array('application/x-tex'),
		'texi' 		=>	array('application/x-texinfo'),
		'texinfo' 	=>	array('application/x-texinfo'),
		'man' 		=>	array('application/x-troff-man'),
		'me' 		=>	array('application/x-troff-me'),
		'ms' 		=>	array('application/x-troff-ms'),
		'roff' 		=>	array('application/x-troff'),
		't' 		=>	array('application/x-troff'),
		'tr' 		=>	array('application/x-troff'),
		'ustar' 	=>	array('application/x-ustar'),
		'src' 		=>	array('application/x-wais-source'),
		'zip' 		=>	array('application/x-zip-compressed','application/octet-stream','application/zip'),
		'tsi' 		=>	array('audio/TSP-audio'),
		'au' 		=>	array('audio/basic'),
		'snd' 		=>	array('audio/basic'),
		'kar' 		=>	array('audio/midi'),
		'mid' 		=>	array('audio/midi'),
		'midi' 		=>	array('audio/midi'),
		'mp2' 		=>	array('audio/mpeg'),
		'mp3' 		=>	array('application/x-download',"audio/mpeg",'application/force-download','audio/mp3'),
		'mpga' 		=>	array('audio/mpeg'),
		'au' 		=>	array('audio/ulaw'),
		'aif' 		=>	array('audio/x-aiff'),
		'aifc' 		=>	array('audio/x-aiff'),
		'aiff' 		=>	array('audio/x-aiff'),
		'm3u' 		=>	array('audio/x-mpegurl'),
		'wax' 		=>	array('audio/x-ms-wax'),
		'wma' 		=>	array('audio/x-ms-wma'),
		'rpm' 		=>	array('audio/x-pn-realaudio-plugin'),
		'ram' 		=>	array('audio/x-pn-realaudio'),
		'rm' 		=>	array('audio/x-pn-realaudio'),
		'ra' 		=>	array('audio/x-realaudio'),
		'wav' 		=>	array('audio/x-wav'),
		'pdb' 		=>	array('chemical/x-pdb'),
		'xyz' 		=>	array('chemical/x-pdb'),
		'ras' 		=>	array('image/cmu-raster'),
		'gif' 		=>	array('image/gif'),
		'ief' 		=>	array('image/ief'),
		'jpe' 		=>	array('image/jpeg'),
		'jpeg' 		=>	array('image/jpeg'),
		'jpg' 		=>	array('image/jpeg','image/pjpeg','image/jpg','application/octet-stream'),
		'png' 		=>	array('image/png'),
		'tif' 		=>	array('image/tiff'),
		'tiff' 		=>	array('image/tiff'),
		'ras' 		=>	array('image/x-cmu-raster'),
		'pnm' 		=>	array('image/x-portable-anymap'),
		'pbm' 		=>	array('image/x-portable-bitmap'),
		'pgm' 		=>	array('image/x-portable-graymap'),
		'ppm' 		=>	array('image/x-portable-pixmap'),
		'rgb' 		=>	array('image/x-rgb'),
		'xbm' 		=>	array('image/x-xbitmap'),
		'xpm' 		=>	array('image/x-xpixmap'),
		'xwd' 		=>	array('image/x-xwindowdump'),
		'iges' 		=>	array('model/iges'),
		'igs' 		=>	array('model/iges'),
		'mesh' 		=>	array('model/mesh'),
		'msh' 		=>	array('model/mesh'),
		'silo' 		=>	array('model/mesh'),
		'vrml' 		=>	array('model/vrml'),
		'wrl' 		=>	array('model/vrml'),
		'css' 		=>	array('text/css'),
		'htm' 		=>	array('text/html'),
		'html' 		=>	array('text/html'),
		'asc' 		=>	array('text/plain'),
		'c' 		=>	array('text/plain'),
		'cc' 		=>	array('text/plain'),
		'f90' 		=>	array('text/plain'),
		'f' 		=>	array('text/plain'),
		'h' 		=>	array('text/plain'),
		'hh' 		=>	array('text/plain'),
		'm' 		=>	array('text/plain'),
		'txt' 		=>	array('text/plain'),
		'rtx' 		=>	array('text/richtext'),
		'rtf' 		=>	array('text/rtf'),
		'sgm' 		=>	array('text/sgml'),
		'sgml' 		=>	array('text/sgml'),
		'tsv' 		=>	array('text/tab-separated-values'),
		'jad' 		=>	array('text/vnd.sun.j2me.app-descriptor'),
		'etx' 		=>	array('text/x-setext'),
		'xml' 		=>	array('text/xml'),
		'dl' 		=>	array('video/dl'),
		'fli' 		=>	array('video/fli'),
		'flv' 		=>	array('video/x-flv','video/flv','application/download','application/octet-stream',),
		'gl' 		=>	array('video/gl'),
		'mp2' 		=>	array('video/mpeg'),
		'mp4' 		=>	array('video/mp4'),
		'mpe' 		=>	array('video/mpeg'),
		'mpeg' 		=>	array('video/mpeg'),
		'mpg' 		=>	array('video/mpeg'),
		'mov' 		=>	array('application/download','video/quicktime',),  
		'qt' 		=>	array('video/quicktime'),
		'viv' 		=>	array('video/vnd.vivo'),
		'vivo' 		=>	array('video/vnd.vivo'),
		'fli' 		=>	array('video/x-fli'),
		'asf' 		=>	array('video/x-ms-asf'),
		'asx' 		=>	array('video/x-ms-asx'),
		'wmv' 		=>	array('video/x-ms-wmv','application/download',),
		'wmx' 		=>	array('video/x-ms-wmx'),
		'wvx' 		=>	array('video/x-ms-wvx'),
		'avi' 		=>	array('video/x-msvideo','application/octet-stream','application/download',),
		'movie' 	=>	array('video/x-sgi-movie'),
		'mime' 		=>	array('www/mime'),
		'ice' 		=>	array('x-conference/x-cooltalk'),
		'vrm' 		=>	array('x-world/x-vrml'),
		'vrml' 		=>	array('x-world/x-vrml'),
		'ods'		=> 	array('application/vnd.oasis.opendocument.spreadsheet'),
		'odt'		=> 	array('application/vnd.oasis.opendocument.text'),
		'doc' 		=>	array('application/msword','application/octet-stream','application/vnd.ms-word'),
		'docx' 		=>	array('application/msword','application/octet-stream','application/vnd.ms-word','application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
		'xls' 		=>	array('application/excel','application/octet-stream','application/vnd.ms-excel'),
		'xlsx' 		=>	array('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/excel','application/octet-stream','application/vnd.ms-excel'),
		
		'csv'		=> 	array('application/vnd.ms-excel','text/csv','application/octet-stream','text/comma-separated-values'),
		'epub'		=> 	array('application/epub+zip'),
		'mobi'		=> 	array('application/octet-stream'),
	); 
	
	/**
	* zakladni index
	*/
	public function index(){
		if ($this->request->here != '/filemanager/'){
			$this->redirect('/filemanager/');
		}
		//pr($this->request->here);
	}
	
	/**
	* pripojeni k FTP serveru
	*/
	private function connectFtp(){
		$ftp_server = $this->ftpData->ftp['host'];
		$this->ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
		$login = ftp_login($this->ftp_conn, $this->ftpData->ftp['user'], $this->ftpData->ftp['password']);
	}
	
	/**
	* ziskani dat z FTP serveru dle cesty
	*/
	public function getFtpList($path=null){
		
		for ($i=0;$i<func_num_args();$i++) $arguments[] = func_get_arg($i);
		//pr($arguments);
		
		if (!empty($arguments)){
			$this->ftpArgumentsArray = $arguments;
			$this->ftpArguments = implode('/',$arguments).'/';
			
		}
		//pr(FTP_PATH);
		//pr($this->ftpPath);
		//pr($this->ftpArguments);die();
		
		
		
		$this->connectFtp();
		/*
		if (!isset($this->ftpPath)){
			$this->ftpPath = ".".FTP_PATH;
		
		} else {
			$this->ftpPath = '.'.FTP_PATH.$this->ftpArguments;
			//pr($this->ftpPath);
			
		}
		
		$pos = strpos($this->ftpPath,FTP_PATH	);
		if (!empty($this->ftpPath) && $pos === false) {
		
			die(json_encode(['result'=>false,'message'=>'Nelze zmenit adresar']));
		} else {
		};
		*/
		//pr($this->ftpPath);
		$file_list = $this->listDetailed($this->ftpArguments);
		die(json_encode(['result'=>true,'currentPath'=>($this->ftpArguments)?$this->ftpArguments:'filemanager/getFtpList/','data'=>$file_list,'path'=>'/uploaded/'.$this->ftpArguments]));
	
	}
	
	/**
	* naformatovani vypisu souboru z FTP
	*/
	private function listDetailed($directory = '.') {
		if (empty($directory)){
			$rootDirectory = true;
		}
		$directory = FTP_PATH.$directory;
		//pr($directory);
		if (is_array($children = @ftp_rawlist($this->ftp_conn, $directory))) {
            //pr($children);
			if (isset($rootDirectory)){
				unset($children[0]);
				unset($children[1]);
			}
			$items = array();
            foreach ($children as $child) {
                $chunks = preg_split("/\s+/", $child);
                list($item['rights'], $item['number'], $item['user'], $item['group'], $item['size'], $item['month'], $item['day'], $item['time']) = $chunks;
                $item['type'] = $chunks[0]{0} === 'd' ? 'directory' : 'file';
                $item['size'] = $this->FileSizeConvert($item['size']);
				
				
				array_splice($chunks, 0, 8);
                $ext = explode('.',implode(" ", $chunks));
				
				//pr($ext);
				$item['ext'] = (!empty($ext[1])?$ext[1]:'dir'); 
				$name = implode(" ", $chunks);
				$item['name'] = $name;
				$item['pathName'] = $name;
/*
				if ($item['name'] == '..'){
					unset($this->ftpArgumentsArray[count($this->ftpArgumentsArray)-1]);
					//pr($this->ftpArgumentsArray);
					if (count($this->ftpArgumentsArray)>0){
						$this->ftpArgumentsPath = implode('/',$this->ftpArgumentsArray).'/';
					} else {
						$this->ftpArgumentsPath = '';
						$item['pathName'] = '';
					}
					
					//pr($this->ftpArguments);	
				}
				*/
				$item['path'] = $this->ftpArguments.$item['name'].(($item['type'] == 'directory')?'/':'');
				//pr($this->ftpPath);
				//pr(FTP_PATH);
				//pr(WEB_PATH);
				//pr($this->ftpPath);
				//$item['webPath'] = strtr($this->ftpPath,[$this->ftpPath=>WEB_PATH]).$item['name'].(($item['type'] == 'directory')?'/':'');
				
				$item['webPath'] = WEB_PATH.$this->ftpArguments.$item['name'].(($item['type'] == 'directory')?'/':'');
				$item['pathPreview'] = WEB_PATH.$this->ftpArguments.$item['name'].(($item['type'] == 'directory')?'/':'');
				$item['pathCurrent'] = $this->ftpPath.(($item['type'] == 'directory')?'/':'');
				
				if ($name != '.'){
					if ($this->ftpPath == '.'.FTP_PATH && $name == '..'){
						
					} else {
						$items[] = $item;
					
					}
					
				}
            }
			//pr($items);
            $dirList = [];
			$fileList = [];
			$ftpList = [];
			foreach($items AS $i){
				if ($i['type'] == 'directory'){
					$dirList[] = $i;
				} else {
					$fileList[] = $i;
				}
			}
				$ftpList = array_merge($dirList,$fileList);
				
			
			//pr($fileList);
			if (isset($this->request->query['debug']))
			pr($ftpList);
			
			return $ftpList;
        }

        // Throw exception or return false < up to you
    }
	
	/**
	* create folder
	*/
	public function createFolder(){
		$this->request->data['folder'] = strtr($this->removeDiacritics($this->request->data['folder']),['.'=>'']);
		$this->connectFtp();
		//pr($this->request->data['folder']);
		if (@ftp_mkdir($this->ftp_conn,FTP_PATH.$this->request->data['path'].$this->request->data['folder'])){
		
			die(json_encode(['result'=>true,'message'=>'Složka byla vytvořena']));	
		} else {
			die(json_encode(['result'=>false,'message'=>'Chyba vytvoření složky']));
			
		}
	}
	
	/**
	* rename file / folder
	*/
	public function renameFile(){
		$this->request->data['newFile'] = $this->removeDiacritics($this->request->data['newFile']);
		$this->request->data['newName'] = $this->removeDiacritics($this->request->data['newName']);
		//pr(FTP_PATH.$this->request->data['oldFile']);die();
		$this->connectFtp();
		
		if (@ftp_rename($this->ftp_conn,FTP_PATH.$this->request->data['oldFile'],FTP_PATH.$this->request->data['newFile'])){
		
			die(json_encode(['result'=>true,'message'=>'Přejmenováno...','file'=>$this->request->data['newName']]));	
		} else {
			die(json_encode(['result'=>false,'message'=>'Chyba přejmenování']));
			
		}
	}
	
	/**
	upload files
	*/
	public function upload(){
		//pr($_FILES);
		foreach($_FILES['file']['name'] AS $k=>$file){
			$this->filename = $this->removeDiacritics($file);
			$this->data_upload['type'] = $_FILES['file']['type'][$k];
			$this->tmpFilename 		= $_FILES['file']['tmp_name'][$k];
			if (!$this->file_ext_verify()){
				die(json_encode(array('result'=>false,'message'=>$this->error_message)));
			} else {
				$this->connectFtp();
				//pr($this->request->query['path']);
				$destination = FTP_PATH.$this->request->query['path'].$this->filename;
				//pr($destination);
				//pr($this->tmpFilename);
				$fp = fopen($this->tmpFilename,"r");			
				$result = ftp_fput($this->ftp_conn, $destination, $fp, FTP_BINARY);
				//pr($result);
				//pr($this->ftp_conn);
			}
		
		//pr($this->request->query);
		//pr($_FILES);
		}
		die(json_encode(['result'=>true,'message'=>'Soubory nahrány...','currentPath'=>FTP_PATH.$this->request->query['path']]));
	}
	
	/**
	* kontrola souboru
	*/
	function file_ext_verify(){
		$ext = strtolower($this->getFileExt($this->filename));
		//pr($this->data_upload['type']); pr($this->mime_types[$ext]); die();
		//pr($this->enabledExt);
		//pr($this->data_upload['type']);
		//pr($this->data_upload);
		if (in_array($ext, $this->enabledExt) || $ext == 'xlsm'){  
			if (isset($this->mime_types[$ext])){
				if (in_array($this->data_upload['type'], $this->mime_types[$ext])){
					return true;
				}
			}
		} else {
			//pr($this->mime_types);
			$this->error_message = 'Nepovolený formát souboru: "'.$ext.'" - "'.$this->data_upload['type'].'"';
			//$this->error_message = 'Chyba soubor existuje';
								
			return false;
		}
	}
	
	/**
	* ziskani pripomy
	*/
	function getFileExt($file){
		$dots = explode('.',$file);
		return $dots[sizeOf($dots)-1];
	}
	
	
	/**
	* delete file / folder
	*/
	public function deleteFile($isDir=null){
		$this->connectFtp();
		
		//pr($this->request->data['file']);
		if($isDir){
			
			if (@ftp_rmdir($this->ftp_conn, FTP_PATH.$this->request->data['file'])){
				die(json_encode(['result'=>true,'message'=>'Smazáno...']));
				
			} else {
				die(json_encode(['result'=>false,'message'=>'Chyba smazani']));
				
			}
		} else {
		
			
			if (@ftp_delete($this->ftp_conn, FTP_PATH.$this->request->data['file'])){
				die(json_encode(['result'=>true]));
				
			} else {
				die(json_encode(['result'=>false,'message'=>'Chyba smazani']));
				
			}
		}
	}
	
	/**
	* remove diacritics
	*/
	
	private function removeDiacritics($url){
		

	$table = array(
			' '=>'-', 'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj', 'Ž'=>'Z', 'ž'=>'z', 'C'=>'C', 'c'=>'c', 'C'=>'C', 'c'=>'c',
			'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
			'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
			'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
			'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
			'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
			'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
			'ÿ'=>'y', 'R'=>'R', 'r'=>'r', "'"=>'-', '"'=>'-'
		);

	$string = strtr($url, $table);
	return $string;

	}
	
	/**
	* prevod jednotek velikosti souboru
	*/
	private function FileSizeConvert($bytes){
		if ($bytes == 0){
			$result = 0;
		} else {
		
		$bytes = floatval($bytes);
			$arBytes = array(
				0 => array(
					"UNIT" => "TB",
					"VALUE" => pow(1024, 4)
				),
				1 => array(
					"UNIT" => "GB",
					"VALUE" => pow(1024, 3)
				),
				2 => array(
					"UNIT" => "MB",
					"VALUE" => pow(1024, 2)
				),
				3 => array(
					"UNIT" => "KB",
					"VALUE" => 1024
				),
				4 => array(
					"UNIT" => "B",
					"VALUE" => 1
				),
			);

		foreach($arBytes as $arItem)
		{
			if($bytes >= $arItem["VALUE"])
			{
				$result = $bytes / $arItem["VALUE"];
				$result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
				break;
			}
		}
		}
		return $result;
		
	}
	
	/**
	* example CK editor
	*/
	public function ckeditor(){
		
	}
	
	
	
	
	
}
