<?php

namespace FileManager\Controller;

use App\Controller\AppController as BaseController;
use Cake\Event\Event;
use Cake\Cache\Cache;

class AppController extends BaseController
{
	
	/**
	* inicializace session ftp
	*/
	public function initSession(){
		
		require_once('../plugins/FileManager/config/config.php');
		
		// toto je docasne vytvoreni session, SESSION_NAME je z /config/config.php
		$this->Session->write(SESSION_NAME,(object)[
			'ftp'=>[
				'host'=>'109.123.216.75',
				'user'=>'fastestsolution_car',
				'password'=>'car159',
			],
			'domain'=>'http://p-carprogram.fastest.cz'
		]);
		
		// pokud neni session s ftp udaji
		if(!$this->Session->check(SESSION_NAME)){
			die('Neni nastaven uzivatel pro zobrazeni souboru');
			
		} else {
			$this->ftpData = $this->Session->read(SESSION_NAME);
		}
		
	}
	
	
	public function beforeFilter(Event $event){
		$this->viewBuilder()->layout('fileManager');
		$this->Session = $this->request->session();
		$this->initSession();
		
		
		if (in_array($this->request->action, ['renameFile','upload'])) {
			$this->eventManager()->off($this->Csrf);
		}		
		$this->Security->config('unlockedActions', [
			'renameFile',
			'deleteFile',
			'createFolder',
			'upload',
		]);	
		
		return parent::beforeFilter($event);
	}
	
	public function initialize(){
		parent::initialize();
		
		
		
	}
	
	
}
