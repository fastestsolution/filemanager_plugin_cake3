<div id="fstAlert" class="alert ">
  
</div>
<h1>Správa souborů</h1>
<div class="row">
	<div class="col col-sm-12 col-md-9 col-lg-8">
		<button id="newFolder" class="btn btn-primary btn-sm" >Nová složka</button>
		<div class="tips">
			Tip: Soubor vyberete kliknutím na červenou ikonu před názvem souboru.
		</div>
		<div id="currentPath">Právě se nacházíte: <span id="currentPathShow"></span></div>
		<input type="text" placeholder="Název složky (uložíte stisknutím ENTER)" class="form-control none" id="newFolderName"></input>
		
		<table class="table-hover table-striped" id="filesTable">
			<thead>
				<tr>
					<th></th>
					<th>Soubor</th>
					<th class="text_right">Velikost</th>
					<th class="text_right">Možnosti</th>
				</tr>
			</thead>
			<tbody>
				<tr data-template>
					<td class="text_center"><a href="#"  class="fa fa-check-circle fileSelect type_{{type}}" data-file="{{path}}" data-path="{{pathCurrent}}" data-webPath="{{webPath}}" data-name="{{name}}" title="Vybrat"></a></td>
					<td>
						<span class="file_icon ico_{{ext}}">{{ext}}</span>
						<a class="file_change type_{{type}}" data-path-preview="{{pathPreview}}" href="/filemanager/getFtpList/{{path}}">{{name}}</a>
					</td>
					<td class="text_right text_light">{{size}}</td>
					<td class="text_right">
						<a href="#" class="fa fa-edit fileRename" data-file="{{path}}" data-path="{{pathCurrent}}" data-name="{{name}}" title="Přejmenovat"></a>
						<a href="#" class="fa fa-close fileDelete type_{{type}}" data-file="{{path}}" title="Smazat"></a>
					</td>
				</tr>
			
			</tbody>
		</table>
	</div>
	<div class="col col-sm-12 col-md-3 col-lg-4">
		<div id="fileUploader">
		<form method="post" action="/filemanager/upload/" enctype="multipart/form-data" id="uploadForm">
			<div>
				<div class="formRow">
					<input type="file" id="file" name="file[]" multiple>
					<div id="fileSelectOver">Vybrat soubor</div>
				</div>

				<div class="text_center">
					<input type="submit" name="upload" id="uploadSubmit" value="Nahrát na server" class="btn btn-primary">
				</div>
			</div>
		</form>
		</div>
		<div id="previewFile">
			<div id="previewFileAbs">
			
				<div id="previewFileResize" class="fa fa-external-link"></div>
				<iframe src="" id="previewFileFrame"></iframe>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
//<![CDATA[
   function getUrlParam( paramName ) {
            var reParam = new RegExp( '(?:[\?&]|&)' + paramName + '=([^&]+)', 'i' );
            var match = window.location.search.match( reParam );

            return ( match && match.length > 1 ) ? match[1] : null;
        }
        // Simulate user action of selecting a file to be returned to CKEditor.
        
	function selectFile(){	
        alert('');
		var funcNum = getUrlParam( 'CKEditorFuncNum' );
			//console.log(funcNum);
			var fileUrl = '/path/to/file.txt';
        window.opener.CKEDITOR.tools.callFunction(fileUrl);
        console.log(window.opener);
	}
//]]>
</script>