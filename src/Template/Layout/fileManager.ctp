<!DOCTYPE html>
<html>
<head>
  <?= $this->Html->charset() ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Správce souborů</title>
  <?php /* ?>
  <script type="text/javascript">less = { env: 'development' };</script>
  <?= $this->Html->css("ajax.css", ["rel" => "stylesheet/less"]); ?>
  <?php */ ?>
  <link rel="stylesheet" type="text/css" href="/FileManager/css/bootstrap/bootstrap-grid.min.css"  media="screen" />
  <link rel="stylesheet" type="text/css" href="/FileManager/css/bootstrap/bootstrap.min.css"  media="screen" />
  <link rel="stylesheet/less" type="text/css" href="/FileManager/css/style.less"  media="screen" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  
  <?= $this->Html->script("/FileManager/js/less"); ?>
  <?= $this->Html->script("/FileManager/js/tempo"); ?>
  <?= $this->Html->script("https://scripts.fastesthost.cz/js/mootools1.4/core1.6.js"); ?>
  <?= $this->Html->script("https://scripts.fastesthost.cz/js/mootools1.4/c_more1.6.js"); ?>
  <?= $this->Html->script("/FileManager/js/fileManager"); ?>
  <?= $this->Html->script("/FileManager/js/uploader/MultipleFileInputs"); ?>
  <?= $this->Html->script("/FileManager/js/uploader/FileUpload"); ?>
  <?= $this->Html->script("/FileManager/js/uploader/RequestFile"); ?>
  <?= $this->Html->script("/FileManager/js/uploader/Iframe"); ?>
  
  <?= $this->fetch('meta') ?>
  <?= $this->fetch('css') ?>
  <?= $this->fetch('script') ?>
  <?php
		if (isset($scripts)){if (is_array($scripts)){foreach ($scripts as $link){echo $this->Html->script($link);}} else {echo $this->Html->script($scripts);}}
		if (isset($styles)){if (is_array($styles)){foreach ($styles as $style){echo $this->Html->css($style);} } else {echo $this->Html->css($styles);}} 
	?>
</head>
<body class="ajax <?= strtolower($this->request->controller."-".$this->request->action) ?>">
<div class="container full">
	<div class="row">
		<div class="col col-12">
			<?= $this->fetch('content') ?>
		</div>
	</div>
</div>
</body>
</html>